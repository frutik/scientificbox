# About #

Virtual machine with pre-configured scipy, numpy, python-matplot, R and R-studio

# How to install #

## Install Vagrant 1.3.* ##

http://docs.vagrantup.com/v2/installation/index.html

## Install VirtualBox ##

https://www.virtualbox.org/wiki/Downloads

## Install Homebrew ##

http://brew.sh/

## Install ansible ##

```
brew install python
sudo pip install ansible --upgrade
```

# How to use #

- Download source, unzip, go inside extracted folder
- Start VM:

```
vagrant up
```

- Wait until it finish his bootstrap (first time it can take a long time)
- You can go inside vm:

```
vagrant ssh
```
 [Probably you have to create private/public ssh keys and copy public key into VM](https://help.github.com/articles/generating-ssh-keys)
 
 ```
 ssh-copy-id -i ~/.ssh/id_rsa.pub vagrant@10.0.6.9
 ```

- R-Studio is available on http://10.0.6.9:8787 user vagrant, password vagrant